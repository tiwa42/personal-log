package com.tiwa.pl;

import java.util.List;

public interface LogRepository {

    LogEntry createEntry();

    List<LogEntry> listEntrys();

    LogEntry getEntryByName(String name);

    void updateEntry(LogEntry logEntry);

    void deleteEntrys(LogEntry logEntry);

    void updateTemplate(LogEntry logEntry);

    LogEntry getTemplate();
}